const globby = require('globby');
const fs = require('fs');
const promisify = require('util').promisify;
const mkdirp = require('mkdirp');
const path = require('path');
const concat = require('concat');
const babel = require("@babel/core");
const uglify = require("uglify-js2");
const del = require('del');
const purgecss = require('purgecss');
const CleanCSS = require('clean-css');
const nunjucks = require('nunjucks');
const htmlclean = require('htmlclean');
const watch = require('node-watch');
const serve_static = require('serve-static');
const http = require('http');
const finalhandler = require('finalhandler');
const sharp = require('sharp');
const tailwindcss = require('tailwindcss');
var sass = require('node-sass');


// const latex = require('node-latex');
// const latex_templates = nunjucks.configure('templates', {
//     tags: {
//         blockStart: '<%',
//         blockEnd: '%>',
//         variableStart: '<$',
//         variableEnd: '$>',
//         commentStart: '<#',
//         commentEnd: '#>'
//     }
// });
//
// latex_templates.addFilter('render_bold', function (str) {
//     return str.replace(/\*([^*]+)\*/g, "\\textbf{$1}");
// });
//
// latex_templates.addFilter('map_join', function (obj, key, str) {
//     let list = obj.map((x) => x[key]);
//     return list.join(str);
// });


nunjucks.configure("templates");
const cleancss = new CleanCSS({level: 2});
const stat = promisify(fs.stat);
const write_file = promisify(fs.writeFile);
const read_file = promisify(fs.readFile);
const readdir = promisify(fs.readdir);
const compile_sass = promisify(sass.render);


const async_map = async (list, f) => {
    return await Promise.all(list.map(f)).catch(console.error);
};


const read_json = async (path) => {
    let src = await read_file(path);
    return JSON.parse(src);
};


const copy = async (src, dst) => {
    src = Array.isArray(src) ? src[0] : src;
    console.log("COPY", src, dst);
    let x = await read_file(src);
    await write_file(dst, x);
};

const copy_resize = async (src, dst) => {
    src = Array.isArray(src) ? src[0] : src;

    await copy(src, dst);
    let resized_dst = dst.replace(/\.([^/.]+)$/, ".small.$1");
    await sharp(src)
        .resize(350)
        .toFile(resized_dst);

};


const copy_all = async (src, dst) => {
    await mkdirp(dst);
    return await async_map(src, async (p) => {
        let name = path.basename(p);
        // console.log(p, name);
        let my_dst = path.join(dst, name);
        return await copy(p, my_dst);
    });

};


// const _render_latex = (input, dst) => {
//     return new Promise((resolve, reject) => {
//         const output = fs.createWriteStream(dst);
//         const pdf = latex(input);
//
//         pdf.pipe(output);
//         pdf.on('error', err => {
//             console.error(err);
//             reject(err);
//         });
//         pdf.on('finish', () => {
//             console.log('PDF generated!');
//             resolve();
//         })
//     });
// };


const serve_static_folder = (path, port) => {

    // Serve up public/ftp folder
    let serve = serve_static(path, {'index': ['index.html', 'index.htm']});

    let server = http.createServer(function onRequest(req, res) {
        serve(req, res, finalhandler(req, res))
    });

    server.listen(port);
};


const _get_last_modified_time = async (p) => {
    if (fs.existsSync(p)) {
        return new Date((await stat(p)).mtime);
    } else {
        return 0;
    }
};


const get_last_modified_time = async (paths) => {
    const mtimes = await async_map(paths, async (p) => _get_last_modified_time(p));
    return Math.max.apply(null, mtimes);
};

class Module {
    constructor(name, src, dst, compile_dev, compile_dst, dependencies = null) {
        this.name = name;
        this.src = src;
        this.dst = dst;

        this.dst_folder = path.dirname(dst);
        this.dst_name = path.basename(dst);
        this.dependencies = dependencies || [];

        this._compile_dev = compile_dev;
        this._compile_dst = compile_dst;
    }

    async initialize() {
        this.src = await globby(this.src);
        this.src = this.src.sort((a, b) => a.match(/[^\/]+/)[0].localeCompare(a.match(/[^\/]+/)[0]));
    }

    async compile_dev() {
        await mkdirp(path.join("dev", this.dst_folder));

        const dst_path = path.join("dev", this.dst);

        const dst_time = await get_last_modified_time([dst_path]);
        const src_time = await get_last_modified_time(this.src.concat(this.dependencies));

        // console.log(this.src, src_time, dst_time, dst_path);


        if (src_time > dst_time) {
            console.log("Compiling dev '" + this.name + "'");
            await this._compile_dev(this.src, dst_path);
        }
    }

    async compile_dst() {
        await this.compile_dev();

        await mkdirp(path.join("dst", this.dst_folder));
        const dev_path = path.join("dev", this.dst);
        const dst_path = path.join("dst", this.dst);

        const dst_time = await get_last_modified_time([dst_path]);
        const src_time = await get_last_modified_time([dev_path]);


        if (src_time > dst_time) {
            console.log("Compiling dst '" + this.name + "'");
            await this._compile_dst(dev_path, dst_path);
        }
    }
}


const compile_js_dev = async (src_files, dst) => {
    const src_code = await concat(src_files);
    //let code = babel.transform(src_code, {"presets": [path.join(__dirname, "node_modules/babel-preset-env"), {
    //        "modules": false
    //    }]}).code;
    let code = (await babel.transformAsync(src_code, {
        "presets": [path.join(__dirname, "../@babel/preset-env")]
    })).code;

    await write_file(dst, code);
};

const _compile_css_dev = async (file) => {
    // return (await compile_sass({'file': file})).css;
    let css = await read_file(file);
    css = (await tailwindcss.process(css, {from: file, theme: "tailwind.config.js"})).css;
    return (await compile_sass({'data': css})).css;
};

const compile_css_dev = async (src_files, dst) => {
    const css = await async_map(src_files, _compile_css_dev);
    await write_file(dst, css.join("\n"));
};

const compile_css_dst = async (src, dst) => {
    let html_pages = globby.sync([path.join(process.cwd(), 'dev/**/*.html'), path.join(process.cwd(), 'dev/*.html')]);

    let code = new purgecss({
        content: html_pages,
        css: [src],
        extractors: [
            {
                extractor: class {
                    static extract(content) {
                        return content.match(/[A-z0-9-:\/]+/g) || []
                    }
                },
                extensions: ['html', 'js', 'php', 'vue']
            }
        ]
    }).purge()[0].css;

    code = cleancss.minify(code).styles;

    await write_file(dst, code);
};


const compile_js_dst = async (src, dst) => {
    let code = uglify.minify(src).code;
    await write_file(dst, code);
};

const render_html = (data) => {
    return async (src, dst) => {
        let code = await read_file(src[0]);

        let page_data = null;

        if (data !== undefined) {
            page_data = await read_json(data);
        }

        code = nunjucks.renderString(code.toString('utf8'), page_data);

        code = htmlclean(code);
        await write_file(dst, code);
    };
};

const render_latex = (data) => {
    return async (src, dst) => {
        let code = await read_file(src[0]);

        let page_data = null;

        if (data !== undefined) {
            page_data = await read_json(data);
        }

        code = latex_templates.renderString(code.toString('utf8'), page_data);
        // console.log(code);
        // code = htmlclean(code);
        await _render_latex(code, dst);
    };
};

const js_lib = (name, src, dst) => {
    return new Module(name, "src/" + src, dst, compile_js_dev, compile_js_dst);
};

const css_lib = (name, src, dst) => {
    return new Module(name, "src/" + src, dst, compile_css_dev, compile_css_dst, ["tailwind.config.js"]);
};

const html_page = (src, data) => {
    return new Module(src, "src/" + src, src, render_html(data), copy);
};

const latex_file = (src, data) => {
    return new Module(src, "src/" + src, src.replace(".tex", ".pdf"), render_latex(data), copy);
};


// const cfg = JSON.parse(fs.readFileSync('bundler.json', 'utf8'));
//
//
// const js_libs = cfg["js"].map((o) => {
//     return js_lib(o.dst, o.src, o.dst)
// });
//
// const css_libs = cfg["css"].map((o) => {
//     return css_lib(o.dst, o.src, o.dst)
// });
//
// const html_pages = cfg["html"].map((o) => {
//     if (typeof o === 'string' || o instanceof String) {
//         return html_page(o);
//     } else {
//         return html_page(o.page, o.data);
//     }
// });

// const latex_files = [
//     latex_file("resume.tex","data.json")
// ];


class MultiModule {
    constructor(name, src, dst, compile_dev, compile_dst) {
        this.name = name;
        this.src = src;
        this.dst = dst;

        this.dst_folder = path.dirname(dst);
        this.dst_name = path.basename(dst);

        this._compile_dev = compile_dev;
        this._compile_dst = compile_dst;
    }

    async initialize() {
        this.src = await globby(this.src);
        this.modules = this.src.map((s) => new Module("", s, s.replace("src/", ""), this._compile_dev, this._compile_dst));
        await async_map(this.modules, async (m) => m.initialize());
    }

    async compile_dev() {
        await async_map(this.modules, async (m) => m.compile_dev())
    }

    async compile_dst() {
        await async_map(this.modules, async (m) => m.compile_dst())
    }
}


class RoshBundler {
    constructor() {
        this.modules = [];
    }

    add_js_lib(src, dst) {
        this.modules.push(js_lib(dst, src, dst));
    }

    add_css_lib(src, dst) {
        this.modules.push(css_lib(dst, src, dst));
    }

    add_html_page(page, data) {
        this.modules.push(html_page(page, data));
    }

    add_copy(src) {
        this.modules.push(new Module(src, "src/" + src, src, copy, copy));
    }

    add_copy_folder(folder) {
        this.modules.push(new MultiModule(folder, ["src/" + folder + "/**/*"], folder, copy, copy));
    }


    add_image_folder(folder) {
        this.modules.push(new MultiModule(folder, ["src/" + folder + "/**/*.+(jpg|jpeg|png|svg|gif|webp)"], folder, copy_resize, copy_resize));
    }

    async build() {
        mkdirp("dev");
        mkdirp("dst");

        await async_map(this.modules, async (module) => {
            await module.initialize().catch(console.error);
            await module.compile_dev().catch(console.error);
        });

        await async_map(this.modules, async (module) => {
            await module.compile_dst().catch(console.error);
        });
    }

    async run() {
        if (process.argv.length > 2) {
            if (process.argv[2] === "clean") {
                console.log("Cleaning up");
                await del(["dev", "dst"]);
            }
            if (process.argv[2] === "init") {
                console.log("Initializing empty project");
                mkdirp("dev");
                mkdirp("dst");
                mkdirp("templates");
                mkdirp("src");
            }
            if (process.argv[2] === "build") {
                await this.build();
            }
            if (process.argv[2] === "serve") {
                await this.build();
                serve_static_folder("dst", 3000);
                console.log("Project available at http://localhost:3000");
            }
        } else {
            console.log("Initial build...");
            await this.build();

            let self = this;

            console.log("Creating watcher...");
            watch("src", {recursive: true}, function (evt, name) {
                let p = self.build();
                p.catch((e) => console.error(e));
            });

            fs.watchFile("tailwind.config.js", function (evt, name) {
                let p = self.build();
                p.catch((e) => console.error(e));
            });

            serve_static_folder("dev", 3000);
            console.log("Project available at http://localhost:3000");
        }
    }
}

exports.RoshBundler = RoshBundler;

