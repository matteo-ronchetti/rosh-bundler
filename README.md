# rosh-bundler

## Installation
install with
```bash
npm install git+https://gitlab.com/matteo-ronchetti/rosh-bundler.git
```

## Example
Create a file called `bundler.js` with the following content
```js
// import rosh-bundler
const RoshBundler = require("rosh-bundler").RoshBundler;

// instantiate the bundler
let bundler = new RoshBundler();

// run the bundler
bundler.run();
```
and run the command `node bundler.js init` that will create an empty project 

```js
const RoshBundler = require("rosh-bundler").RoshBundler;

let bundler = new RoshBundler();

bundler.add_js_lib(["js/base/*.js"], "js/base.js");

bundler.add_html_page(annotate.html);

bundler.run();
```